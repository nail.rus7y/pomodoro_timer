import { makeAutoObservable, runInAction } from "mobx";

export const TimerStatus = {
	editMode: "editMode",
	active: "active",
	pending: "pending",
};

function createTimerStore() {
	return makeAutoObservable({
		status: TimerStatus.pending,
		minutes: 0,
		seconds: 0,
		start() {
			this.status = TimerStatus.active;
		},
		stop() {
			this.status = TimerStatus.pending;
		},
		edit() {
			this.status = TimerStatus.editMode;
		},
		tick() {
			setTimeout(() => {
				runInAction(() => {
					if (!this.isTimeIsUp && this.isActive) {
						if (this.seconds === 0) {
							this.minutes = this.minutes - 1;
							this.seconds = 59;
						} else {
							this.seconds = this.seconds - 1;
						}
						this.tick();
					}
				});
			}, 1000);
		},
		setTime({ minutes, seconds }: { minutes: number; seconds: number }) {
			this.setMinutes(minutes);
			this.setSeconds(seconds);
		},
		setMinutes(minutes: number) {
			this.minutes = minutes;
		},
		setSeconds(seconds: number) {
			this.seconds = seconds;
		},
		get isActive() {
			return this.status === TimerStatus.active;
		},
		get isTimeIsUp() {
			return this.seconds === 0 && this.minutes === 0;
		},
		get isPending() {
			return this.status === TimerStatus.pending;
		},
		get isEditMode() {
			return this.status === TimerStatus.editMode;
		},
	});
}
export const timerStore = createTimerStore();
