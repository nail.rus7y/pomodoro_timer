import { timerStore } from "./mobx/TimerStore/TimerStore";
import { autorun, reaction } from "mobx";
import { alertRing, minutesElement, secondsElement, start, stop, startBtn } from "./handlers";

function initPomodoroTimer() {
	autorun(() => {
		minutesElement.value = timerStore.minutes < 10 ? `0${timerStore.minutes}` : `${timerStore.minutes}`;
		secondsElement.value = timerStore.seconds < 10 ? `0${timerStore.seconds}` : `${timerStore.seconds}`;
	});
	reaction(
		() => timerStore.isActive,
		(value) => value && timerStore.tick()
	);
	reaction(
		() => timerStore.isEditMode,
		(value) => {
			minutesElement.disabled = !value;
			secondsElement.disabled = !value;
		}
	);
	reaction(
		() => timerStore.isActive,
		(value) => {
			if (value) {
				startBtn.removeEventListener("click", start);
				startBtn.addEventListener("click", stop);
				startBtn.innerHTML = "stop";
			}
		}
	);
	reaction(
		() => timerStore.isPending || timerStore.isEditMode || timerStore.isTimeIsUp,
		(value) => {
			if (value) {
				startBtn.removeEventListener("click", stop);
				startBtn.addEventListener("click", start);
				startBtn.innerHTML = "start";
			}
		}
	);
	reaction(
		() => !timerStore.isTimeIsUp,
		(value) => value && alertRing.classList.remove("ending")
	);
	reaction(
		() => timerStore.isTimeIsUp && !timerStore.isEditMode,
		(value) => value && alertRing.classList.add("ending")
	);

	timerStore.setTime({ minutes: 0, seconds: 10 });
}

initPomodoroTimer();
