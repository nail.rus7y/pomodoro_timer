import { timerStore } from "./mobx/TimerStore/TimerStore";

export const alertRing: HTMLDivElement = document.querySelector(".ring")!;
export const startBtn: HTMLButtonElement = document.querySelector(".start")!;
export const settingsBtn: HTMLButtonElement = document.querySelector(".settings")!;
export const minutesElement: HTMLInputElement = document.querySelector(".minutes input")!;
export const secondsElement: HTMLInputElement = document.querySelector(".seconds input")!;

export function start() {
	timerStore.start();
}

export function stop() {
	timerStore.stop();
}

startBtn.addEventListener("click", () => timerStore.start());
settingsBtn.addEventListener("click", () => timerStore.edit());
minutesElement.addEventListener("keyup", (e) => {
	const target = e.target as HTMLInputElement;
	let value = /^\d+$/.test(target.value) ? Number(target.value) : 0;
	value = value > 59 ? 59 : value;
	value = value < 0 ? 0 : value;
	timerStore.setMinutes(value);
	minutesElement.value = String(value);
});
secondsElement.addEventListener("keyup", (e) => {
	const target = e.target as HTMLInputElement;
	let value = /^\d+$/.test(target.value) ? Number(target.value) : 0;
	value = value > 59 ? 59 : value;
	value = value < 0 ? 0 : value;
	timerStore.setSeconds(value);
	secondsElement.value = String(value);
});
