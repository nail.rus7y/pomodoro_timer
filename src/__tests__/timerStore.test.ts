import {timerStore} from "../mobx/TimerStore/TimerStore";
import {reaction} from "mobx";

describe("TimerStore | Pomodoro timer", () => {
	beforeAll(() => {
		reaction(
			() => timerStore.isActive,
			(value) => value && timerStore.tick()
		);
		jest.useFakeTimers();
		jest.spyOn(global, "setTimeout");
	});

	afterEach(() => {
		jest.clearAllTimers();
		timerStore.stop();
	});

	it("should set time equal to 21 minutes and 47 seconds", () => {
		timerStore.setTime({ minutes: 21, seconds: 47 });
		expect(timerStore.minutes).toEqual(21);
		expect(timerStore.seconds).toEqual(47);
	});

	it("should set isActive to true when the timer is started", () => {
		timerStore.setTime({ minutes: 1, seconds: 0 });
		timerStore.start();
		expect(timerStore.isActive).toBeTruthy();
	});

	it("should change seconds of timer", () => {
		timerStore.setTime({ minutes: 1, seconds: 30 });
		timerStore.start();
		jest.runOnlyPendingTimers();
		expect(timerStore.seconds).toBe(29);
		jest.runOnlyPendingTimers();
		expect(timerStore.seconds).toBe(28);
	});

	it("should change seconds and minutes of timer", () => {
		timerStore.setTime({ minutes: 1, seconds: 0 });
		timerStore.start();
		jest.runOnlyPendingTimers();
		expect(timerStore.minutes).toBe(0);
		expect(timerStore.seconds).toBe(59);
	});

	it("should set isPending to true when the timer stopped", () => {
		timerStore.setTime({ minutes: 1, seconds: 0 });
		timerStore.start();
		timerStore.stop();
		expect(timerStore.isPending).toBeTruthy();
	});

	it("should set timeIsUp to true when we have no time", () => {
		timerStore.setTime({ minutes: 0, seconds: 0 });
		expect(timerStore.isTimeIsUp).toBeTruthy();
	});

	it("should set editMode", () => {
		timerStore.edit();
		expect(timerStore.isEditMode).toBeTruthy();
	})

	it("should set timeIsUp when time is over", () => {
		timerStore.setTime({ minutes: 0, seconds: 2 });
		timerStore.start();
		jest.runOnlyPendingTimers();
		jest.runOnlyPendingTimers();
		expect(timerStore.seconds).toEqual(0);
		expect(timerStore.isTimeIsUp).toBeTruthy();
	})
});
