# Pomodoro timer

### What?
There is a simple pomodoro like timer.
### Why?
It's one of the project where I'm practicing with different libraries and frameworks.
### How?
Just run <code>yarn build</code> command and gulp will start the project

And use <code>yarn test</code> for run tests
### List if features
- [x] Start the timer by clicking on the start button.
- [x] Once the user clicks start, the word start will change to stop. Then, the user can click on the stop button to make the timer stop.
- [x] Click on the gear icon to change the length (minutes and seconds) of the timer.
- [x] Once the timer finishes, the ring should change from green to red and an alert message is passed to the browser.
### Used libraries
- Gulp
  * gulp-uglify - minify our js code
  * gulp-clean-css - minify our css 
  * gulp-replace - replace target string in a file
  * watchify - plugin for watching events
  * browserify - combine modules to one file
  * gulp-sourcemaps - creating source maps
  * tsify - compile our typescript, compatibility with browserify 
  * vinyl-buffer -
  * vinyl-source-stream - compatibility with files of browserify
- Eslint
- Prettier
- Mobx
- TypeScript
- Jest
- Babel
