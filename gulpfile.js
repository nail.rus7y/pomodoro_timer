const { parallel, dest, src } = require("gulp");
const uglify = require("gulp-uglify");
const cleanCSS = require("gulp-clean-css");
const replace = require("gulp-replace");
const browserify = require("browserify");
const source = require("vinyl-source-stream");
const tsify = require("tsify");
const watchify = require("watchify");
const sourcemaps = require("gulp-sourcemaps");
const buffer = require("vinyl-buffer");

const watched = watchify(
	browserify({
		basedir: ".",
		debug: true,
		entries: ["src/index.ts"],
		cache: {},
	}).plugin(tsify)
);

const copyStatic = (cb) => {
	src("./index.html")
		.pipe(replace("src/styles/styles.css", "styles.css"))
		.pipe(replace("src/index.ts", "index.js"))
		.pipe(dest("./build"));
	src("./fonts/**").pipe(dest("./build/fonts"));
	src("./images/**").pipe(dest("./build/images"));
	cb();
};

const javascript = (cb) => {
	watched
		.bundle()
		.pipe(source("index.js"))
		.pipe(buffer())
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(uglify())
		.pipe(sourcemaps.write("./"))
		.pipe(dest("./build"));
	cb();
};
const css = (cb) => {
	src("./src/styles/styles.css").pipe(cleanCSS()).pipe(dest("./build"));
	cb();
};

watched.on("update", parallel(javascript, css, copyStatic));

exports.build = parallel(javascript, css, copyStatic);
